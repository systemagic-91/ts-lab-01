var menor: number = Number.MAX_VALUE;

function pow(base:number, expoente:number):number
{
    if (expoente == 0){
        return 1;
    }

    if (expoente == 1) {
        return base;
    }

    let resultado: number = base;

    for (let index = 1; index < expoente; index++) {        
        resultado*= base;
    }

    return resultado;
}

console.log(pow(5,1))