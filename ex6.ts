function getMax(array: number[]): number
{
    let maior: number = Number.MIN_VALUE;

    for (let i: number = 0; i < array.length; i++) {        
        if (array[i] > maior) {
            maior = array[i];
        }        
    }

    return maior;
}

console.log(getMax([0,1,2,3,4,99]));