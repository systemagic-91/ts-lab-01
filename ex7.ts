function frequencia(array:number[]): number{

    const map = Object.create(null);

    for (const num of array){
        if(map[num]){
            map[num] += 1;
        } else {
            map[num] = 1;
        }
    }

    return map;
}

console.log(frequencia([1,1,1,1,2,2,3,0,30,99,0,99,5]));