var i: number = 0;

while (i != 10) {
 i += 0.2;
}

console.log(i);

/**
 * o codigo acima entra em loop infinito 
 * por um problema de arredondamento.
 */