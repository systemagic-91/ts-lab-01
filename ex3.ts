var menor: number = Number.MAX_VALUE;

function min(a:number, b:number):number
{
    if (a < b){
        return a;
    }

    if (b < a){
        return b;
    }

    return -1;
}